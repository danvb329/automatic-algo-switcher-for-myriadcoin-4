<?php
if (!function_exists('json_last_error_msg')) {
    function json_last_error_msg()
    {
        switch (json_last_error()) {
            default:
                return;
            case JSON_ERROR_DEPTH:
                $error = 'Maximum stack depth exceeded';
            break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Underflow or the modes mismatch';
            break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Unexpected control character found';
            break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON';
            break;
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded';
            break;
        }
        return $error;
    }
}

function command($command) {
	@$fp = fsockopen("127.0.0.1", 4028, $errno, $errstr, 30);

	if (!$fp) {
		$error_obj = array("error" => $errstr, "err_no" => $errno);
		$error_json = json_encode($error_obj, true);
		return $error_json;
	} else {
		fwrite($fp, $command);
		$json = '';
		while (!feof($fp)) {
			$json .= fgets($fp, 128);
		}
		fclose($fp);
		return $json;
	}
}

function strip_crap($checkLogin) {
	// This will remove unwanted characters.
	// Check http://www.php.net/chr for details
	for ($i = 0; $i <= 31; ++$i) { 
		$checkLogin = str_replace(chr($i), "", $checkLogin); 
	}
	$checkLogin = str_replace(chr(127), "", $checkLogin);

	// This is the most common part
	// Some file begins with 'efbbbf' to mark the beginning of the file. (binary level)
	// here we detect it and we remove it, basically it's the first 3 characters 
	if (0 === strpos(bin2hex($checkLogin), 'efbbbf')) {
	   $checkLogin = substr($checkLogin, 3);
	}
	
	return $checkLogin;
}

function blocks_per_day($difficulty, $hashes_per_second) {
	$block_time_secs = pow(2, 32) / $hashes_per_second * $difficulty;
	$secs_in_day = 86400;
	$blocks_per_day = $secs_in_day / $block_time_secs;
	return $blocks_per_day;
}

function alive_and_running() {
	$command = array('command' => 'pools');
	$json = json_encode($command);
	$response = command($json);

	$response_obj = json_decode(strip_crap($response), true);
	$alive = false;
	$running = false;
	if(!empty($response_obj['POOLS'])) {
		$running = true;
		$pools = $response_obj['POOLS'];
		foreach($pools as $pool) {
			if($pool['Status'] === 'Alive') {
				$alive = true;
			}
		}
	}
	return array($alive, $running);
}

function quit() {
	$command = array('command' => 'quit');
	$json = json_encode($command);
	$response = command($json);

	return json_decode(strip_crap($response), true);
}

function get_difficulties() {
	$info_json = file_get_contents('http://myriad.theblockexplorer.com/api.php?mode=info');
	return json_decode($info_json);
}

