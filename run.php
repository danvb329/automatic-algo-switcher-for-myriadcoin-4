<?php
require_once 'func.php';
$running = '';

while(true) {
	$info = get_difficulties();
	
	$ini_array = parse_ini_file("algos.ini", true);
	
	$algos = array();
	if($ini_array['scrypt']['enabled']) {
		$algos[$ini_array['scrypt']['filename']] = blocks_per_day($info->difficulty_scrypt, $ini_array['scrypt']['hashrate']);
	}
	if($ini_array['groestl']['enabled']) {
		$algos[$ini_array['groestl']['filename']] = blocks_per_day($info->difficulty_groestl, $ini_array['groestl']['hashrate']);
	}
	if($ini_array['skein']['enabled']) {
		$algos[$ini_array['skein']['filename']] = blocks_per_day($info->difficulty_skein, $ini_array['skein']['hashrate']);
	}
	if($ini_array['qubit']['enabled']) {
		$algos[$ini_array['qubit']['filename']] = blocks_per_day($info->difficulty_qubit, $ini_array['qubit']['hashrate']);
	}
	
	arsort($algos); //reverse sort maintaining association
	list($key) = each($algos); //grab the first
	if($key != $running) { //algo changed
		$quit = quit();
		if($running != '') {
			echo $quit['error'];
		}
		$running = $key; //this is the algo we're running
		exec('start ' . $key);
	}
	
	echo "We're currently running $running\n";
	print_r($algos);

	sleep(180);
}
